using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Walking : MonoBehaviour
{
    public int pvZombie = 3;
    Transform targetToFollow;
    Animator zombie;
    int pvPlayer;
    AudioSource grrr;
    NavMeshAgent myNavMeshAgent;


    // Start is called before the first frame update
    void Start()
    {
        targetToFollow = GameObject.FindGameObjectWithTag("Player").transform;
        zombie = GetComponent<Animator>();
        zombie.SetTrigger("walk");
        grrr = GetComponent<AudioSource>();
        myNavMeshAgent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        pvPlayer = GameObject.FindGameObjectWithTag("Player").GetComponent<Moving>().pv;
        //enemyRigidBody.MovePosition(transform.position + transform.forward * enemySpeed * Time.deltaTime);
        if (pvZombie > 0 && pvPlayer > 0)
        {
            //transform.position = Vector3.MoveTowards(transform.position, targetToFollow.position, Time.deltaTime * enemySpeed);
            myNavMeshAgent.SetDestination(targetToFollow.position);
            //transform.LookAt(targetToFollow);
            zombie.SetTrigger("walk");
        }
        else
        {
            myNavMeshAgent.isStopped = true;
            zombie.SetTrigger("noPunch");
            zombie.SetTrigger("noWalk");
        }

    }

    private void OnTriggerEnter(Collider other)
    {

        switch (other.tag)
        {
            case "Bullet":
                pvZombie--;
                if (pvZombie == 0)
                {
                    zombie.SetTrigger("death");
                    myNavMeshAgent.isStopped = true;
                    grrr.Pause();
                }
                break;
            case "Player":
                if (pvPlayer > 0)
                {
                    zombie.SetTrigger("punch");
                    myNavMeshAgent.isStopped = true;
                }
                if (pvPlayer <= 0)
                {
                    zombie.SetTrigger("noWalk");
                    myNavMeshAgent.isStopped = true;
                }
                break;
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") && pvPlayer > 0)
        {
            zombie.SetTrigger("noPunch");
            zombie.SetTrigger("walk");
            myNavMeshAgent.isStopped = false;
        }
        else
        {
            zombie.SetTrigger("noPunch");
            zombie.SetTrigger("noWalk");
        }

    }
}

