using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    public bool GameIsPaused = false;
    public GameObject pauseMenuUI;
    Canvas canvas;

    private void Start()
    {
        canvas = GameObject.FindGameObjectWithTag("Finish").GetComponent<Canvas>();
        canvas.enabled = false;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                canvas.enabled = false;
                Resume(); 
            }
            else
            {
                canvas.enabled = true;
                Pause();
            }
        }
    }

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        GameObject.FindGameObjectWithTag("Player").GetComponent<Moving>().enabled = true;
        GameObject.FindGameObjectWithTag("GameController").GetComponent<Wave>().enabled = true;
        Debug.Log("Resume");
        Time.timeScale = 1f;
        GameIsPaused = false;

        for(int i = 0; i < GameObject.FindGameObjectsWithTag("Zombie").Length; i++ )
        {
            GameObject.FindGameObjectsWithTag("Zombie")[i].GetComponent<AudioSource>().enabled = true;
        }
        
    }

    void Pause()
    {
        pauseMenuUI.SetActive(true);
        GameObject.FindGameObjectWithTag("Player").GetComponent<Moving>().enabled = false;
        GameObject.FindGameObjectWithTag("GameController").GetComponent<Wave>().enabled = false;
        Time.timeScale = 0f;
        GameIsPaused = true;
        for (int i = 0; i < GameObject.FindGameObjectsWithTag("Zombie").Length; i++)
        {
            GameObject.FindGameObjectsWithTag("Zombie")[i].GetComponent<AudioSource>().enabled = false;
        }
    }

    public void endGame()
    {
        Application.Quit();
    }
}
