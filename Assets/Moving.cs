using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Moving : MonoBehaviour
{
    Camera cam;
    Collider planeCollider;
    RaycastHit hit;
    Ray ray;
    //public float speed  = 5f;
    public int pv = 10;
    float dmgWait = 0.0f; // stock le temps pass� depuis la derniere execution;
    public float delai;	// tu defini l'interval voulu, en seconde.	
    public float epsilon;
    Animator anim;
    public GameObject gun;
    public GameObject bulletObject;
    public Transform canon;

    Vector3 newHit;
    AudioSource soundShoot;
    NavMeshAgent myNavMeshAgent;

    // Start is called before the first frame update
    void Start()
    {
        cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        planeCollider = GameObject.Find("Plane").GetComponent<Collider>();
        anim = GetComponent<Animator>();
        soundShoot = GetComponent<AudioSource>();
        myNavMeshAgent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (pv > 0)
        {
            ray = cam.ScreenPointToRay(Input.mousePosition);
            if (!Input.GetMouseButton(0))
            {
                if (Physics.Raycast(ray, out hit))
                {
                    if (V3Equal(hit.point, transform.position) > epsilon)
                    {
                        anim.SetTrigger("run");
                        myNavMeshAgent.isStopped = false;
                        newHit = hit.point;
                        newHit.y = 5.1f;
                        myNavMeshAgent.SetDestination(hit.point);
                        //transform.position = Vector3.MoveTowards(transform.position, newHit, Time.deltaTime * speed);
                        transform.LookAt(new Vector3(hit.point.x, transform.position.y, hit.point.z));
                    }
                    else
                    {
                        anim.SetTrigger("noRun");
                        myNavMeshAgent.isStopped = true;
                    }
                }

            }
            else
            {
                if (Physics.Raycast(ray, out hit))
                {
                    myNavMeshAgent.isStopped = true;
                    anim.SetTrigger("shoot");
                    transform.LookAt(new Vector3(hit.point.x, transform.position.y, hit.point.z));
                    dmgWait += Time.fixedDeltaTime;  // ajoute a chaque update le temps �coul� depuis le dernier Update		
                    if (dmgWait > delai)
                    {
                        dmgWait = 0;
                        StartCoroutine(shoot());
                        Instantiate(bulletObject, canon.position, transform.rotation);
                    }
                }
            }
        }
        else
        {
            anim.SetTrigger("death");
        }
    }

    public float V3Equal(Vector3 a, Vector3 b)
    {
        return Vector3.SqrMagnitude(a - b);
    }
    IEnumerator shoot()
    {
        Debug.Log("shoot sound");
        soundShoot.Play();
        yield break;
    }
    private void OnTriggerEnter(Collider other)
    {


        if (other.CompareTag("ZombieArm"))
        {
            if (other.GetComponentInParent<Walking>().pvZombie > 0)
            {
                pv--;
                Debug.Log(pv);
            }

        }
        if (pv == 0)
        {
            anim.SetTrigger("death");
            //Destroy(transform.gameObject);
        }
    }
}

