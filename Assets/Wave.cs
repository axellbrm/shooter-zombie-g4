using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wave : MonoBehaviour
{
    public Transform spawnZombie;
    public Transform spawnZombie1;
    public GameObject zombie;
    public int wave;
    private float waveWait = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        wave = 1;
        Instantiate(zombie, spawnZombie.position, transform.rotation);
        Instantiate(zombie, spawnZombie1.position, transform.rotation);
    }

    // Update is called once per frame
    void Update()
    {
        waveWait += Time.fixedDeltaTime;  // ajoute a chaque update le temps �coul� depuis le dernier Update		
        if (waveWait > 50 && wave < 10 )
        {
            wave++;
            waveWait = 0;
            Instantiate(zombie, spawnZombie.position, transform.rotation);
            Instantiate(zombie, spawnZombie1.position, transform.rotation);
        }
    }
}
